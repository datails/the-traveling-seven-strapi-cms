# the-traveling-seven-strapi-cms
This app uses heroku to deploy the CMS. The CMS is hosted at https://travelingseven.herokuapp.com/admin. The current Strapi version is V4. 

## Develop

```bash
docker-compose up
```

Make sure you have all env vars set correctly. Needed env vars in `.env`:

```
HOST=
PORT=
APP_KEYS=
API_TOKEN_SALT=
ADMIN_JWT_SECRET=
JWT_SECRET=
DATABASE_FILENAME=
CLOUDINARY_API_KEY=
CLOUDINARY_API_SECRET=
CLOUDINARY_CLOUD_NAME=
```

## Deploy

```bash
heroku login
git push heroku master # when you have commited something
```

It will now create a build and deploy from your local machine.

