const fs = require('fs');
const path = require('path');

async function provision() {
  const BASE_DIR = path.join('extensions', 'users-permissions', 'config');
  const FILES = await fs.promises.readdir(BASE_DIR);
  const MAPPING = {
    __JWT_SECRET__: process.env.JWT_SECRET
  }
  
  for (const file of FILES) {
    const filePath = path.join(DIR, file);

    let content = await fs.promises.readFile(filePath, {
      encoding: 'utf-8'
    });

    content = Object.keys(MAPPING).reduce((acc, curr) => {
      return acc.replace(new RegExp(curr, 'g'), MAPPING[curr])
    }, content)

    fs.writeFileSync(filePath, content, 'utf8')
  }
}

provision()
