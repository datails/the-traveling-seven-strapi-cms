FROM node:16.19.0

ENV TZ Europe/Amsterdam

WORKDIR /app

COPY . .

RUN yarn && yarn build

EXPOSE 3000

CMD ["yarn", "develop"]
