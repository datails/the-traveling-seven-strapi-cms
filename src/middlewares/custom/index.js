const path = require("path");
const koaStatic = require("koa-static");
const fs = require("fs");

module.exports = strapi => {
  return {
    async initialize() {
      const { maxAge } = strapi.config.middleware.settings.public;

      const basename = "/";
      const dashboardDir = path.resolve(strapi.dir, "app");

      const validRoutes = [
        "^(/fotoboek?\/?.*)$",
        "^(/blog?\/?.*)$",
        "^(\/category?\/?.*)$",
        "^(\/vlog?\/?.*)$",
        "^(\/over-ons?\/?.*)$",
        "^(\/)$"
      ];

      const routeMatch = (routes, routePath) => {
        for ( const i in routes ) {
          const route = routes[i];

          if (routePath.match(route)) {
            return true
          }
        }

        return false
      }

      // Serve dashboard assets.
      strapi.router.get(
        `${basename}*`,
        async (ctx, next) => {
          const routePath = ctx.url.split("?")[0];
          ctx.url = ctx.url.replace(/^\/app/, "");
          if (routeMatch(validRoutes, routePath) || !ctx.url) {
            console.log(routePath)
            ctx.url = basename;
          }
          await next();
        },
        koaStatic(dashboardDir, {
          index: "index.html",
          maxage: maxAge,
          defer: false
        })
      );
    }
  };
};